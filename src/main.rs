extern crate toml;

use std::env;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;
use std::process::exit;

fn main() {
    let args: Vec<String> = env::args().collect();
    let path = args[1].clone();
    let path = Path::new(&path);
    let mut file = File::open(path).expect("Failed to open file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("Failed to read file");
    let mut parser = toml::Parser::new(&contents);

    if parser.parse().is_none() {
        println!("Error parsing {}:", args[1]);
        for error in parser.errors {
            println!("{}", error);
        }
        exit(1);
    }
}
