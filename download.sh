set -eu
outdir="/Users/zofrex/testcrates"

meta="$1"

data=$(gtail -n 1 "$meta")
name=$(echo $data | jq -r ".name")
version=$(echo $data | jq -r ".vers")
url="https://crates.io/api/v1/crates/$name/$version/download"
outfile="$outdir/$name-$version.tar"
if [ ! -f $outfile ]
then
	echo "Downloading $url..."
	curl --silent --fail -L $url -o "$outfile"
else
	echo "Skipping $url..."
fi
