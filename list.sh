set -eu
parallel -j8 ./download.sh -- $(find . -type f -mindepth 2 -not -regex "^\./\.git.*")
